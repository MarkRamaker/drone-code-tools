/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
 #include <posaccf.hpp>
 

/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
PositionAccelerationFilter<T>::PositionAccelerationFilter (T exp, T frq) :
l1 (exp), l2 (exp), l3 (exp), h1 (exp), h2 (exp), h3 (exp) {
    U = (T) 1.0 / frq;
    P1 = 0;
    #if POS_ACC_VER == 0
    P2 = 0;
    #endif
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
PositionAccelerationFilter<T>::PositionAccelerationFilter (T exp, T frq, T pos, T acc) :
#if POS_ACC_VER == 1
// Filter Construction:
//
// pos --------------------------------------------------> l3
// |                                                       |
// |----------------------------------------> l2           |
// |                                          |            |
// |----> dx ----------> l1                   |            |
//                       |                    |            |
// acc -> h1 -> (ix) -> (+) -> h2 -> (ix) -> (+) -> h3 -> (+) -> output
//
// l1 starts at a  spd of value: zero.
l1 (exp, 0, 0), 
// l2 starts at a  pos of value: pos.
l2 (exp, pos, 0), 
// l3 starts at a  pos of value: pos.
l3 (exp, pos, 0), 
// h1 starts at an acc of value: acc.
h1 (exp, acc, 0), 
// h2 starts at a  spd of value: acc * (1 / exp - 1) / frq.
h2 (exp, acc * (1 / exp - 1) / frq, 0),
// h3 starts at a  pos of value: pos + acc * ((1 / exp - 1) / frq)^2. 
h3 (exp, pos + acc * ((1 / exp - 1) / frq) * ((1 / exp - 1) / frq), 0) {

#elif POS_ACC_VER == 2
// Filter Construction:
//
// pos --------------------------------------------------> l3
// |                                                       |
// dx ------------------------------> l2                   |
// |                                  |                    |
// |-------------------> l1           |                    |
//                       |            |                    |
// acc -> h1 -> (ix) -> (+) -> h2 -> (+) -> h3 -> (ix) -> (+) -> output
//
// l1 starts at an spd of value: zero.
l1 (exp, 0, 0), 
// l2 starts at an spd of value: zero.
l2 (exp, 0, 0), 
// l3 starts at an pos of value: pos.
l3 (exp, pos, 0), 
// h1 starts at an acc of value: acc.
h1 (exp, acc, 0), 
// h2 starts at an spd of value: acc * (1 / exp - 1) / frq.
h2 (exp, acc * (1 / exp - 1) / frq, 0),
// h3 starts at an spd of value: zero. 
h3 (exp, 0, 0) {

#else
// Filter Construction:
//
// pos ----------------------------------------------------> l3
// |                                                         |
// |----> dx -------------------------> l2                   |
//        |                             |                    |
//        |-> dx-> l1                   |                    |
//                 |                    |                    |
// acc -> h1 ---> (+) -> h2 -> (ix) -> (+) -> h3 -> (ix) -> (+) -> output
//
// l1 starts at an acc of value: zero.
l1 (exp, 0, 0),
// l2 starts at a  spd of value: zero.
l2 (exp, 0, 0),
// l3 starts at a  pos of value: pos.
l3 (exp, pos, 0),
// h1 starts at an acc of value: acc.
h1 (exp, acc, 0),
// h2 starts at an acc of value: zero.
h2 (exp, 0, 0),
// h3 starts at a  spd of value: zero.
h3 (exp, 0, 0) {

#endif
    U = (T) 1.0 / frq;
    P1 = pos;
    #if POS_ACC_VER == 0
    P2 = 0;
    #endif
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
PositionAccelerationFilter<T>::~PositionAccelerationFilter () {

}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T PositionAccelerationFilter<T>::Filter (T pos, T acc) {    
    #if POS_ACC_VER == 1 
    // Filter Construction:
    //
    // pos --------------------------------------------------> l3
    // |                                                       |
    // |----------------------------------------> l2           |
    // |                                          |            |
    // |----> dx ----------> l1                   |            |
    //                       |                    |            |
    // acc -> h1 -> (ix) -> (+) -> h2 -> (ix) -> (+) -> h3 -> (+) -> output
    //
    T posspd = (pos - P1) / U;
    
    T l1out = l1.FilterLow(posspd);
    T h1out = h1.FilterLow(acc) * (1 / h1.GetExp () - 1) * U;
    T l2out = l2.FilterLow(pos);
    T h2out = h2.FilterLow(l1out + h1out) * (1 / h2.GetExp () - 1) * U;
    T l3out = l3.FilterLow(pos);
    T h3out = h3.FilterHigh(l2out + h2out);
    T r = l3out + h3out;
    
    #elif POS_ACC_VER == 2
    // Filter Construction:
    //
    // pos --------------------------------------------------> l3
    // |                                                       |
    // dx ------------------------------> l2                   |
    // |                                  |                    |
    // |-------------------> l1           |                    |
    //                       |            |                    |
    // acc -> h1 -> (ix) -> (+) -> h2 -> (+) -> h3 -> (ix) -> (+) -> output
    //
    T posspd = (pos - P1) / U;
    
    T l1out = l1.FilterLow(posspd);
    T h1out = h1.FilterLow(acc) * (1 / h1.GetExp () - 1) * U;
    T l2out = l2.FilterLow(posspd);
    T h2out = h2.FilterHigh(l1out + h1out);
    T l3out = l3.FilterLow(pos);
    T h3out = h3.FilterLow(l2out + h2out) * (1 / h3.GetExp () - 1) * U;
    T r = l3out + h3out;
    
    #else
    // Filter Construction:
    //
    // pos ----------------------------------------------------> l3
    // |                                                         |
    // |----> dx -------------------------> l2                   |
    //        |                             |                    |
    //        |-> dx-> l1                   |                    |
    //                 |                    |                    |
    // acc -> h1 ---> (+) -> h2 -> (ix) -> (+) -> h3 -> (ix) -> (+) -> output
    //
    T posacc = ((pos - P1) - P2) / (U * U);
    T posspd = (pos - P1) / U;
    
    T l1out = l1.FilterLow(posacc);
    T h1out = h1.FilterHigh(acc);
    T l2out = l2.FilterLow(posspd);
    T h2out = h2.FilterLow(l1out + h1out) * (1 / h2.GetExp () - 1) * U;
    T l3out = l3.FilterLow(pos);
    T h3out = h3.FilterLow(l2out + h2out) * (1 / h3.GetExp () - 1) * U;
    T r = l3out + h3out;

    P2 = pos - P1;
    #endif
    
                                
    P1 = pos;
    return r;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetExp (T in) {
    this->SetExp1(in);
    this->SetExp2(in);
    this->SetExp3(in);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetExp1 (T in) {
    l1.SetExp (in);
    h1.SetExp (in);
    #if POS_ACC_VER == 1
    // pos --------------------------------------------------> l3
    // |                                                       |
    // |----------------------------------------> l2           |
    // |                                          |            |
    // |----> dx ----------> l1                   |            |
    //                       |                    |            |
    // acc -> h1 -> (ix) -> (+) -> h2 -> (ix) -> (+) -> h3 -> (+) -> output
    //
    h2.SetLow (h1.GetLow () * (1 / h1.GetExp () - 1) * U + l1.GetLow ());
    h3.SetLow (h2.GetLow () * (1 / h2.GetExp () - 1) * U + l2.GetLow ());  
    
    #elif POS_ACC_VER == 2 
    //
    // pos --------------------------------------------------> l3
    // |                                                       |
    // dx ------------------------------> l2                   |
    // |                                  |                    |
    // |-------------------> l1           |                    |
    //                       |            |                    |
    // acc -> h1 -> (ix) -> (+) -> h2 -> (+) -> h3 -> (ix) -> (+) -> output
    //
    h2.SetLow (h1.GetLow () * (1 / h1.GetExp () - 1) * U + l1.GetLow ());
    
    #else
    // Filter Construction pos_acc_ver = 0
    //
    // pos ----------------------------------------------------> l3
    // |                                                         |
    // |----> dx -------------------------> l2                   |
    //        |                             |                    |
    //        |-> dx-> l1                   |                    |
    //                 |                    |                    |
    // acc -> h1 ---> (+) -> h2 -> (ix) -> (+) -> h3 -> (ix) -> (+) -> output
    //
    #endif
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetExp2 (T in) {
    l2.SetExp (in);
    h2.SetExp (in);
    #if POS_ACC_VER == 1
    // pos --------------------------------------------------> l3
    // |                                                       |
    // |----------------------------------------> l2           |
    // |                                          |            |
    // |----> dx ----------> l1                   |            |
    //                       |                    |            |
    // acc -> h1 -> (ix) -> (+) -> h2 -> (ix) -> (+) -> h3 -> (+) -> output
    //
    h3.SetLow (h2.GetLow () * (1 / h2.GetExp () - 1) * U + l2.GetLow ());
    
    #elif POS_ACC_VER == 2 
    //
    // pos --------------------------------------------------> l3
    // |                                                       |
    // dx ------------------------------> l2                   |
    // |                                  |                    |
    // |-------------------> l1           |                    |
    //                       |            |                    |
    // acc -> h1 -> (ix) -> (+) -> h2 -> (+) -> h3 -> (ix) -> (+) -> output
    //
    
    #else
    // Filter Construction pos_acc_ver = 0
    //
    // pos ----------------------------------------------------> l3
    // |                                                         |
    // |----> dx -------------------------> l2                   |
    //        |                             |                    |
    //        |-> dx-> l1                   |                    |
    //                 |                    |                    |
    // acc -> h1 ---> (+) -> h2 -> (ix) -> (+) -> h3 -> (ix) -> (+) -> output
    //
    h3.SetLow (h2.GetLow () * (1 / h2.GetExp () - 1) * U + l2.GetLow ());
    #endif
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetExp3 (T in) {
    l3.SetExp (in);
    h3.SetExp (in);
    #if POS_ACC_VER == 1
    // pos --------------------------------------------------> l3
    // |                                                       |
    // |----------------------------------------> l2           |
    // |                                          |            |
    // |----> dx ----------> l1                   |            |
    //                       |                    |            |
    // acc -> h1 -> (ix) -> (+) -> h2 -> (ix) -> (+) -> h3 -> (+) -> output
    //
    
    #elif POS_ACC_VER == 2 
    //
    // pos --------------------------------------------------> l3
    // |                                                       |
    // dx ------------------------------> l2                   |
    // |                                  |                    |
    // |-------------------> l1           |                    |
    //                       |            |                    |
    // acc -> h1 -> (ix) -> (+) -> h2 -> (+) -> h3 -> (ix) -> (+) -> output
    //
    
    #else
    // Filter Construction pos_acc_ver = 0
    //
    // pos ----------------------------------------------------> l3
    // |                                                         |
    // |----> dx -------------------------> l2                   |
    //        |                             |                    |
    //        |-> dx-> l1                   |                    |
    //                 |                    |                    |
    // acc -> h1 ---> (+) -> h2 -> (ix) -> (+) -> h3 -> (ix) -> (+) -> output
    //

    #endif
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T PositionAccelerationFilter<T>::GetTimeConst1 () {
    return l1.GetTimeConst (U);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T PositionAccelerationFilter<T>::GetTimeConst2 () {
    return l2.GetTimeConst (U);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T PositionAccelerationFilter<T>::GetTimeConst3 () {
    return l3.GetTimeConst (U);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetTimeConst1 (T in) {
    l1.SetTimeConst (in, U);
    SetExp1 (l1.GetExp ());
} 


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetTimeConst2 (T in) {
    l2.SetTimeConst (in, U);
    SetExp2 (l2.GetExp ());
} 


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetTimeConst3 (T in) {
    l3.SetTimeConst (in, U);
    SetExp3 (l3.GetExp ());
} 


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T PositionAccelerationFilter<T>::GetCutOffFreq1 () {
    return l1.GetCutOffFreq (U);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T PositionAccelerationFilter<T>::GetCutOffFreq2 () {
    return l2.GetCutOffFreq (U);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T PositionAccelerationFilter<T>::GetCutOffFreq3 () {
    return l3.GetCutOffFreq (U);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetCutOffFreq1 (T in) {
    l1.SetCutOffFreq (in, U);
    SetExp1 (l1.GetExp ());
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetCutOffFreq2 (T in) {
    l2.SetCutOffFreq (in, U);
    SetExp2 (l2.GetExp ());
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void PositionAccelerationFilter<T>::SetCutOffFreq3 (T in) {
    l3.SetCutOffFreq (in, U);
    SetExp3 (l3.GetExp ());
}
