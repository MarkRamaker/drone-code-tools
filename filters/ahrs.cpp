/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
 #include <ahrs.hpp>
 #include <matrix.hpp>
 

/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
AHRS<T>::AHRS (T expgyr, T expacc, T frq) {
    dt = (T) 1.0 / frq;
    this->expg = expgyr;
    this->expa = expacc;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
AHRS<T>::AHRS (T expgyr, T expacc, T frq, Quat<T> ref, Quat<T> gyro) {
    gb = gyro;
    prev = ref;
    dt = (T) 1.0 / frq;
    this->expg = expgyr;
    this->expa = expacc;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
AHRS<T>::~AHRS () {

}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
Quat<T> AHRS<T>::Filter (T acc [3], T mag [3], T gyro [3]) {    
    MatrixBase<T> accvec (acc, 1, 3);
    MatrixBase<T> magvec (mag, 1, 3);
    Matrix<T> ref (3, 3);
    
    T dot;
    char jmp = 0x00; 
    
    // Normalize all vectors, except the gyro vect
    accvec.InnerProd (accvec, dot);
    if (dot) accvec.ScalarMult (1 / dot, accvec);
    else jmp |= 0x01; // sneaky jmp
    magvec.InnerProd (magvec, dot);
    if (dot) magvec.ScalarMult (1 / dot, magvec);
    else jmp |= 0x02; // sneaky jmp
    accvec.InnerProd (magvec, dot);
    if (dot == (T)1) jmp |= 0x02; // When the mag lines up with acc, trust acc 
    
    Matrix<T> axis_x (1, 3);
    Matrix<T> axis_y (1, 3);
    Matrix<T> axis_z (1, 3);
    
    switch (jmp) {
        case (0x01): // No acc but we can use mag to improve ref
            Quat<T> q0 (0, 0, 0, 1);// Experimental FIXME : mag absolute quat
            Quat<T> q1 (0, mag [0, mag [1,] mag [2);// Experimental FIXME : mag quat
            Quat<T> q2 = q0.DevideBy (q1);// Experimental FIXME
            q2.GetRotationMatrix (ref); // Get orientation based on prev orientation.
            //prev.GetRotationMatrix (ref); // Get orientation based on prev orientation.
            ref.GetRow (2, accvec);
            break;
            
        case (0x02): // No mag but we can use acc to improve ref
            prev.GetRotationMatrix (ref); // Get orientation based on prev orientation.
            ref.GetRow (0, magvec);
            break;
            
        case (0x03): // No acc and no mag, use prev orientation as ref
            prev.GetRotationMatrix (ref); // Get orientation based on prev orientation.
            ref.GetRow (0, magvec);
            ref.GetRow (2, accvec);
            break;
            
        default:
            break;
    }
    // Align all axis to 90deg and create the 3rd axis.
    accvec.LeftCrossProd (magvec, axis_y);
    axis_y.LeftCrossProd (accvec, axis_x);
    accvec.Copy (axis_z);
    
    // Caution makes me normalize the x and y axis again.
    // Might be not neccecary
    axis_x.InnerProd (axis_x, dot);
    axis_x.ScalarMult (1 / dot, axis_x);
    axis_y.InnerProd (axis_y, dot);
    axis_y.ScalarMult (1 / dot, axis_y);
    
    ref.SetRow (0, axis_x); // Row 1 is the east west
    ref.SetRow (1, axis_y); // Row 2 is the north south
    ref.SetRow (2, axis_z); // Row 3 is the up down
    
    // Get our starting quaternions, to be filtered.
    Quat<T> refq (ref);
    #ifdef AHRS_YRP
    Quat<T> spdq (gyro [0] * dt, gyro [1] * dt, gyro [2] * dt); // around y,r,p
    #else
    Quat<T> spdq (gyro [2] * dt, gyro [0] * dt, gyro [1] * dt); // around x,y,z
    #endif

    //gb = gb.GetSlerp (spdq - (refq - pref), expg); // Estimate gyrobias.
    //spdq = spdq - gb; // Deduct gyro bias.
    //prev += spdq; // Integrate gyro.
    prev = prev.GetSlerp (refq, expa); // Correction step towards gravity.
    pref = refq; // Keep current gravity orrientation for differentiation
    
    // Normalize to stay safe.
    gb.Normalize ();
    prev.Normalize ();
    
    return prev;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void AHRS<T>::SetExpAcc (T in) {
   expa = in;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void AHRS<T>::SetExpGyr (T in) {
   expg = in;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T AHRS<T>::GetTimeConstAcc () {
    return dt / expa;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T AHRS<T>::GetTimeConstGyr () {
    return dt / expg;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void AHRS<T>::SetTimeConstAcc (T in) {
    expa = dt / in;
} 


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void AHRS<T>::SetTimeConstGyr (T in) {
    expg = dt / in;
} 


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T AHRS<T>::GetCutOffFreqAcc () {
    return 1 / this->GetTimeConstAcc ();
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
T AHRS<T>::GetCutOffFreqGyr () {
    return 1 / this->GetTimeConstGyr ();
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void AHRS<T>::SetCutOffFreqAcc (T in) {
    this->SetTimeConstAcc (1 / in);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T>
void AHRS<T>::SetCutOffFreqGyr (T in) {
    this->SetTimeConstGyr (1 / in);
}
