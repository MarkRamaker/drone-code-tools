/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
 #include <esf.hpp>
 
 /**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
ExponentialSmoother<T, D>::ExponentialSmoother () {
    
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
ExponentialSmoother<T, D>::ExponentialSmoother (D exp) {
    this->exponent = exp;
    this->prevLowOut = (T)0;
    this->prevHighOut = (T)0;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
ExponentialSmoother<T, D>::ExponentialSmoother (D exp, T prevLow, T prevHigh) {
    this->exponent = exp;
    this->prevLowOut = prevLow;
    this->prevHighOut = prevHigh;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
ExponentialSmoother<T, D>::~ExponentialSmoother () {

}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
void ExponentialSmoother<T, D>::Filter (T in) {
#ifdef __OLDVER
    this->prevLowOut *= 1 - this->exponent;
    this->prevLowOut += in * this->exponent;
    this->prevHighOut = in - this->prevLowOut;
#else
    this->prevHighOut = in - this->prevLowOut;
    this->prevLowOut += this->prevHighOut * this->exponent;
    this->prevHighOut = in - this->prevLowOut;
#endif
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
T ExponentialSmoother<T, D>::FilterLow (T in) {
    this->Filter (in);
    return this->prevLowOut;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
T ExponentialSmoother<T, D>::FilterHigh (T in) {
    this->Filter (in);
    return this->prevHighOut;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
T ExponentialSmoother<T, D>::GetLow () {
    return this->prevLowOut;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
T ExponentialSmoother<T, D>::GetHigh () {
    return this->prevHighOut;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
void ExponentialSmoother<T, D>::SetExp (D in) {
    this->exponent = in;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
D ExponentialSmoother<T, D>::GetExp () {
    return exponent;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
void ExponentialSmoother<T, D>::SetLow (T in) {
    this->prevLowOut = in;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
D ExponentialSmoother<T, D>::GetTimeConst (D dt) {
    return dt / this->exponent; 
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
void ExponentialSmoother<T, D>::SetTimeConst (D in, D dt) {
    this->SetExp (dt / in);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
D ExponentialSmoother<T, D>::GetCutOffFreq (D dt) {
    return 1 / this->GetTimeConst (dt);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D> 
void ExponentialSmoother<T, D>::SetCutOffFreq (D in, D dt) {
    this->SetTimeConst (1 / in, dt);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
MultiExponentialSmoother<T, D, N>::MultiExponentialSmoother (D exp) : ExponentialSmoother<T, D> (exp) {
    this->prev = 0;
    for (unsigned int i = 0; i < N - 1; i++) filterArray [i] = ExponentialSmoother<T, D> (exp);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
MultiExponentialSmoother<T, D, N>::MultiExponentialSmoother (D exp, T prevLow, T prevHigh) : ExponentialSmoother<T, D> (exp, prevLow, prevHigh) {
    this->prev = prevLow;
    for (unsigned int i = 0; i < N - 1; i++) filterArray [i] = ExponentialSmoother<T, D> (exp, prevLow, prevHigh);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
MultiExponentialSmoother<T, D, N>::~MultiExponentialSmoother () {

}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
void MultiExponentialSmoother<T, D, N>::Filter (T in) {
    T juggler = in;
    this->prev = in;
    for (unsigned int i = 0; i < N - 1; i++) juggler = filterArray [i].FilterLow (juggler);
    ExponentialSmoother<T, D>::Filter(juggler);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
T MultiExponentialSmoother<T, D, N>::FilterLow (T in) {
    this->Filter (in);
    return ExponentialSmoother<T, D>::GetLow ();
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
T MultiExponentialSmoother<T, D, N>::FilterHigh (T in) {
    this->Filter (in);
    return in - ExponentialSmoother<T, D>::GetLow ();
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
T MultiExponentialSmoother<T, D, N>::GetLow () {
    return ExponentialSmoother<T, D>::GetLow ();
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
T MultiExponentialSmoother<T, D, N>::GetHigh () {
    return this->prev - ExponentialSmoother<T, D>::GetLow ();
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
void MultiExponentialSmoother<T, D, N>::SetExp (D in) {
    for (unsigned int i = 0; i < N - 1; i++) filterArray [i].SetExp (in);
    ExponentialSmoother<T, D>::SetExp (in);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class D, unsigned int N> 
void MultiExponentialSmoother<T, D, N>::SetLow (T in) {
    this->prev = in;
    for (unsigned int i = 0; i < N - 1; i++) filterArray [i].SetLow (in);
    ExponentialSmoother<T, D>::SetLow (in);
}


//end
