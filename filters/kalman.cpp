/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <kalman.hpp>
#include <cmath>
using namespace std;

#ifndef UNSAFE_MATRIX
#define SAFE(x) x
#else
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0.0
#define ONE   (T)1.0
#define TWO   (T)2.0
#define THREE (T)3.0
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2

/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
Kalman<T, S, dim>::Kalman 
		(void (*getFunction) (S delta, MatrixBase<T> &F), 
		void (*getNoise) (S delta, MatrixBase<T> &Q)) :
		x (this->xstore, dim, ONE), P (this->Pstore, dim, dim)  {

	for (unsigned int r = DIM_1; r < dim; r++) {
		for (unsigned int c = DIM_1; c < dim; c++) Pstore [r * dim + c] = ZERO;
		xstore [r] = ZERO;
	}
	this->timeTravel = ZERO;
	this->getFunction = getFunction;
	this->getNoise = getNoise;
}

/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
void Kalman<T, S, dim>::Predict (MatrixBase<T> &F, MatrixBase<T> &Q, MatrixBase<T> &B, MatrixBase<T> &u) {
	T FPstore [dim * dim];
	T FTstore [dim * dim];
	T Fxstore [dim];
	MatrixBase<T> FP (FPstore, dim, dim);
	MatrixBase<T> FT (FTstore, dim, dim);
	MatrixBase<T> Fx (Fxstore, dim, ONE);
	// x = Fx + Bu
	this->x.RightMult (F, Fx);
	u.RightMult (B, this->x);
	this->x.Addition (Fx, this->x);
	// P = FPFT + Q
	this->P.RightMult (F, FP);
	F.Transposed (FT);
	FP.LeftMult (FT, this->P);
	this->P.Addition (Q, this->P);
}

/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
void Kalman<T, S, dim>::Residual (MatrixBase<T> &y, MatrixBase<T> &z, MatrixBase<T> &H) {
	T Hxstore [z.GetRows () * z.GetCols ()];
	MatrixBase<T> Hx (Hxstore, z.GetRows (), z.GetCols ());
	// y = z - Hx
	this->x.RightMult (H, Hx);
	z.LeftSubtract (Hx, y);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
void Kalman<T, S, dim>::Update (MatrixBase<T> &y, MatrixBase<T> &R, MatrixBase<T> &H) {
	T Kstore [H.GetCols () * H.GetRows ()];
	MatrixBase<T> K (Kstore, H.GetCols (), H.GetRows ());

	{
		// S = HPHT + R
		T HTstore [H.GetCols () * H.GetRows ()];
		T Sstore [R.GetRows () * R.GetCols ()];
		T SIstore [R.GetRows () * R.GetCols ()];
		T HPstore [H.GetRows () * H.GetCols ()];
		T PHTstore [H.GetCols () * H.GetRows ()];
		MatrixBase<T> HT (HTstore, H.GetCols (), H.GetRows ());
		MatrixBase<T> Sn (Sstore, R.GetRows (), R.GetCols ());
		MatrixBase<T> SI (SIstore, R.GetRows (), R.GetCols ());
		MatrixBase<T> HP (HPstore, H.GetRows (), H.GetCols ());
		MatrixBase<T> PHT (PHTstore, H.GetCols (), H.GetRows ());
		H.LeftMult (this->P, HP); // H * P
		H.Transposed (HT); // HT
		HP.LeftMult (HT, Sn); // H * P * HT
		Sn.Addition (R, Sn); // S = HPHT + R
		// K = PHTS^-1
		HT.RightMult (this->P, PHT); // P * HT
		Sn.Inverted (SI); // S^-1
		SI.RightMult (PHT, K); // P * HT * S^-1
	}
	{
		// x = x + Ky
		T xastore [this->x.GetRows () * this->x.GetCols ()];
		MatrixBase<T> xa (xastore, this->x.GetRows (), this->x.GetCols ());
		K.LeftMult (y, xa); // K * y
		xa.Addition (this->x, this->x); // x + K * y
	}
	{
		// P = P - KHP
		T KHstore [K.GetRows () * H.GetCols ()];
		T KHPstore [dim * dim];
		MatrixBase<T> KH (KHstore, K.GetRows (), H.GetCols ());
		MatrixBase<T> KHP (KHPstore, dim, dim);
		K.LeftMult (H, KH); // K * H
		KH.LeftMult (this->P, KHP); // K * H * P
		KHP.RightSubtract (this->P, this->P); // P - K * H * P
	}
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
Kalman<T, S, dim>::~Kalman () {

}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
S Kalman<T, S, dim>::GetTimeTravel () {
	return this->timeTravel;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
int Kalman<T, S, dim>::SetP (MatrixBase<T> &in) {
	return in.Copy (this->P);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
int Kalman<T, S, dim>::Setx (MatrixBase<T> &in) {
	return in.Copy (this->x);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
int Kalman<T, S, dim>::GetP (MatrixBase<T> &out) {
	return this->P.Copy (out);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
int Kalman<T, S, dim>::Getx (MatrixBase<T> &out) {
	return this->x.Copy (out);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
void Kalman<T, S, dim>::Filter (MatrixBase<T> &z, MatrixBase<T> &R, MatrixBase<T> &H, S timeStamp) {
	T ustore [dim] = {ZERO};
	T Bstore [dim * dim] = {ZERO};
	MatrixBase<T> u (ustore, dim, ONE);
	MatrixBase<T> B (Bstore, dim, dim);
	
	return this->Filter (z, R, H, B, u, timeStamp);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
void Kalman<T, S, dim>::Filter (MatrixBase<T> &z, KPerf &RD, MatrixBase<T> &H, S timeStamp) {
	T ustore [dim] = {ZERO};
	T Bstore [dim * dim] = {ZERO};
	MatrixBase<T> u (ustore, dim, ONE);
	MatrixBase<T> B (Bstore, dim, dim);
	
	return this->Filter (z, RD, H, B, u, timeStamp);
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
void Kalman<T, S, dim>::Filter (MatrixBase<T> &z, MatrixBase<T> &R, MatrixBase<T> &H, MatrixBase<T> &B, MatrixBase<T> &u, S timeStamp) {
	T Qstore [dim * dim];
	T Fstore [dim * dim];
	T ystore [z.GetRows () * z.GetCols ()];
	MatrixBase<T> Q (Qstore, dim, dim);
	MatrixBase<T> F (Fstore, dim, dim);
	MatrixBase<T> y (ystore, z.GetRows (), z.GetCols ());
	
	if (timeStamp > this->prevTime) {
		this->getFunction (timeStamp - this->prevTime, F);
		this->getNoise (timeStamp - this->prevTime, Q);
		this->Predict (F, Q, B, u);
		this->prevTime = timeStamp;
	} 
	if (H.GetRows () != ZERO && H.GetCols () != ZERO) {
		this->Residual (y, z, H);
		this->Update (y, R, H);
	}
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
void Kalman<T, S, dim>::Filter (MatrixBase<T> &z, KPerf &RD, MatrixBase<T> &H, MatrixBase<T> &B, MatrixBase<T> &u, S timeStamp) {
	T Qstore [dim * dim];
	T Fstore [dim * dim];
	T ystore [z.GetRows () * z.GetCols ()];
	T Rstore [z.GetRows () * z.GetRows ()] = {ZERO};
	MatrixBase<T> Q (Qstore, dim, dim);
	MatrixBase<T> F (Fstore, dim, dim);
	MatrixBase<T> y (ystore, z.GetRows (), z.GetCols ());
	MatrixBase<T> R (Rstore, z.GetRows (), z.GetRows ());
	
	
	if (timeStamp > this->prevTime) {
		this->getFunction (timeStamp - this->prevTime, F);
		this->getNoise (timeStamp - this->prevTime, Q);
		this->Predict (F, Q, B, u);
		this->prevTime = timeStamp;
	} 
	if (H.GetRows () != ZERO && H.GetCols () != ZERO) {
		this->Residual (y, z, H);
		RD.Internal (*this, H, y, R);
		this->Update (y, R, H);
	}
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
void Kalman<T, S, dim>::KPerf::Internal (Kalman<T, S, dim> &f, MatrixBase<T> &H, MatrixBase<T> &y, MatrixBase<T> &R) {
	/*
		step 1: find affected dims
		step 2: precorrect avg and sqr using list and affected dims
		step 3: clear affected dims
		step 4: update avg, sqr and list based on y
		step 5: update avg and sqr recount
		step 6: generate P update and R
		step 7: update write index
		step 8: wrap write index and refresh avg and sqr
		step 9: update P
	*/
	T modF;
	char affectedDims [dim] = {0}; // Keep track of the affected dims.
	T Hmstore [H.GetRows () * H.GetCols ()];
	T HmPstore [H.GetRows () * H.GetCols ()];
	T Pmstore [f.P.GetRows () * f.P.GetCols ()];
	MatrixBase<T> Hm (Hmstore, H.GetRows (), H.GetCols ()); // H Modifier
	MatrixBase<T> HmP (HmPstore, H.GetRows (), H.GetCols ()); // H Modifier times P
	MatrixBase<T> Pm (Pmstore, f.P.GetRows (), f.P.GetCols ()); // P Modifier
	MatrixBase<T> HT (Hmstore, H.GetCols (), H.GetRows ()); // H Modifier Transpose


	// step 1: find affected dims
	// step 2: precorrect avg and sqr using list and affected dims
	// step 3: clear affected dims
	for (unsigned int Hc = DIM_1; Hc < H.GetCols (); Hc++) {
		affectedDims [Hc] = 0;
		if (this->lengths [Hc] == ZERO) continue; // Skip non tracked variables.

		for (unsigned int Hr = DIM_1; Hr < H.GetRows (); Hr++) {
			if (H.GetElement (Hr, Hc) == ZERO) continue; // Skip non updated variables.
			affectedDims [Hc] = 1; // This dim is affected.
			this->avg [Hc] -= this->lists [Hc][this->write [Hc]] / this->lengths [Hc]; // precorrect avg.
			this->sqr [Hc] -= this->lists [Hc][this->write [Hc]] * this->lists [Hc][this->write [Hc]]; // precorrect sqr.
			this->lists [Hc][this->write [Hc]] = ZERO; // clear affected dims.
			break; // Next dim.
		}
	}
     
	// step 4: update avg, sqr and list based on y
	// step 5: update avg and sqr recount
	unsigned int yr = DIM_1;
	for (unsigned int Hc = DIM_1; Hc < H.GetCols (); Hc++) {
		if (this->lengths [Hc] == ZERO) continue; // Skip non tracked variables.

		for (unsigned int Hr = DIM_1; Hr < H.GetRows (); Hr++) {
			if (H.GetElement (Hr, Hc) == ZERO) continue; // Skip non updated variables.
			this->lists [Hc][this->write [Hc]] += H.GetElement (Hr, Hc) * y.GetElement (yr, DIM_1); // update list. 
		}
		this->avg [Hc] += this->lists [Hc][this->write [Hc]] / this->lengths [Hc]; // update avg.
		this->sqr [Hc] += this->lists [Hc][this->write [Hc]] * this->lists [Hc][this->write [Hc]]; // update sqr.
		this->avgRecounts [Hc] += this->lists [Hc][this->write [Hc]] / this->lengths [Hc]; // update avgRecount.
		this->sqrRecounts [Hc] += this->lists [Hc][this->write [Hc]] * this->lists [Hc][this->write [Hc]]; // update sqrRecount.
		yr++; // Next y dimension.
	}

	// step 6: generate P update and R
	// step 7: update write index
	// step 8: wrap write index and refresh avg and sqr
	unsigned int Rc = DIM_1;
	for (unsigned int Hc = DIM_1; Hc < H.GetCols (); Hc++) {
		for (unsigned int Hr = DIM_1; Hr < H.GetRows (); Hr++) {
			Hm.SetElement (Hr, Hc, ZERO); // Zero out the Hm matrix.
			if (H.GetElement (Hr, Hc) == ZERO) continue; // Skip non updated variables.
			// Accumulate Variance data in R.
			R.SetElement (Rc, Rc, R.GetElement (Rc, Rc) + this->GetVar (Hc) / H.GetElement (Hr, Hc));
			// Calculate this distance factor in STDEV.
			modF = abs(this->GetAvg (Hc)) / sqrt (f.P.GetElement (Hc, Hc));
			  
			// Map distance to a scale value.
			if (modF > this->Ustarts [Hc]) {
				if (modF < this->Uends [Hc]) {
					modF = this->Umods [Hc] * ((modF - this->Ustarts [Hc]) / (this->Uends [Hc] - this->Ustarts [Hc]));
				} else modF = this->Umods [Hc];
			} else if (modF < this->Lstarts [Hc]) {
				if (modF > this->Lends [Hc]) {
					modF = this->Lmods [Hc] * ((modF - this->Lstarts [Hc]) / (this->Lends [Hc] - this->Lstarts [Hc]));
				} else modF = this->Lmods [Hc];
			} else modF = ZERO;
			// Set element for Hm
			Hm.SetElement (Hr, Hc, modF);
		}

		// Continue if no wrap.
		if (!affectedDims [Hc]) continue; // Skip non tracked variables.
		Rc++; // New R dimension.
		if (++ this->write [Hc] < this->lengths [Hc]) continue;

		// Wrap and refresh avg and sqr.
		this->write [Hc] = DIM_1; // Wrap
		this->avg [Hc] = this->avgRecounts [Hc]; // Refresh avg.
		this->sqr [Hc] = this->sqrRecounts [Hc]; // Refresh sqr.
		this->avgRecounts [Hc] = ZERO; // Restart avgRecount.
		this->sqrRecounts [Hc] = ZERO; // Restart aqrRecount.
	}

	// step 9: update P
	Hm.LeftMult (f.P, HmP);
	H.Transposed (HT);
	HmP.RightMult (HT, Pm);
	Pm.Addition(f.P, f.P);
	// Done
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
Kalman<T, S, dim>::KPerf::KPerf (T * store, unsigned int lengths [dim], T startVars [dim]) {
	unsigned int totalLength = ZERO;
	for (unsigned int d = ZERO; d < dim; d++) {
		this->lists [d] = store;
		this->write [d] = ZERO;
		this->lengths [d] = lengths [d];
		this->avg [d] = ZERO;
		this->sqr [d] = startVars [d] * lengths [d];
		this->avgRecounts [d] = ZERO;
		this->sqrRecounts [d] = ZERO;
		// If this dim isn't tracked we don't need to set the rest.
		if (!lengths [d]) continue; 
		this->Ustarts [d] = DEFAULT_UPPER_START;
		this->Uends [d] = DEFAULT_UPPER_END;
		this->Umods [d] = DEFAULT_UPPER_MOD;
		this->Lstarts [d] = DEFAULT_LOWER_START;
		this->Lends [d] = DEFAULT_LOWER_END;
		this->Lmods [d] = DEFAULT_LOWER_MOD;
		totalLength += lengths [d];
		store += lengths [d];
		T setList = sqrt (startVars [d]);
		for (unsigned int i = ZERO; i < lengths [d]; i += TWO) this->lists [d] [i] = setList;
		for (unsigned int i = ONE; i < lengths [d]; i += TWO) this->lists [d] [i] = -setList;
	}
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
int Kalman<T, S, dim>::KPerf::SetDimMods (unsigned int d, T Ustart, T Uend, T Umod, T Lstart, T Lend, T Lmod) {
	if (d < dim) {
		this->Ustarts [d] = Ustart;
		this->Uends [d] = Uend;
		this->Umods [d] = Umod;

		this->Lstarts [d] = Lstart;
		this->Lends [d] = Lend;
		this->Lmods [d] = Lmod;
		return 0;
	}
	return -1;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
T Kalman<T, S, dim>::KPerf::GetAvg (unsigned int d) {
	if (d > dim) return ZERO;
	return this->avg [d];
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
T Kalman<T, S, dim>::KPerf::GetVar (unsigned int d) {
	if (d > dim) return ZERO;
	if (this->lengths [d] == ZERO) return ZERO;
	return this->sqr [d] / this->lengths [d] - this->avg [d] * this->avg [d];
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T, class S, unsigned int dim>
Kalman<T, S, dim>::KPerf::~KPerf () {

}

// End

