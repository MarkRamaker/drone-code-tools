/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef KALMAN_HPP
#define KALMAN_HPP 1
#include <matrix.hpp>

// Default scaling values.
#define DEFAULT_UPPER_START 1
#define DEFAULT_UPPER_END 3
#define DEFAULT_UPPER_MOD 0.05
#define DEFAULT_LOWER_START 0.5
#define DEFAULT_LOWER_END 0
#define DEFAULT_LOWER_MOD -0.05



/**
 * @brief Template class for Kalman objects.
 * T is the type of the state values, O is the type of the filter structure and S is the time system.
 * (note: O must support multiplication and addition with T, and O must support all operators with itself and S)
 */
template <class T, class S, unsigned int dim>
class Kalman {
private:
	T xstore [dim];
	T Pstore [dim * dim];
	MatrixBase<T> x;
	MatrixBase<T> P;
	S timeTravel;
	S prevTime;
	void (*getFunction) (S delta, MatrixBase<T> &F);
	void (*getNoise) (S delta, MatrixBase<T> &Q);
	void Predict (MatrixBase<T> &F, MatrixBase<T> &Q, MatrixBase<T> &B, MatrixBase<T> &u);
	void Update (MatrixBase<T> &z, MatrixBase<T> &R, MatrixBase<T> &H);
	void Residual (MatrixBase<T> &y, MatrixBase<T> &z, MatrixBase<T> &H);
public:
	/**
	 * @brief Template class for Kalman objects.
	 * T is the type of the state values, O is the type of the filter structure and S is the time system.
	 * (note: O must support multiplication and addition with T, and O must support all operators with itself and S)
	 */
	class KPerf {
	private:
		friend class Kalman<T, S, dim>;
		T* lists [dim];
		unsigned int write [dim];
		unsigned int lengths [dim];
		T avg [dim];
		T sqr [dim];
		T avgRecounts [dim];
		T sqrRecounts [dim];
		T Ustarts [dim];
		T Uends [dim];
		T Umods [dim];
		T Lstarts [dim];
		T Lends [dim];
		T Lmods [dim];
		void Internal (Kalman<T, S, dim> &f, MatrixBase<T> &H, MatrixBase<T> &y, MatrixBase<T> &R);
	public:
		KPerf (T * store, unsigned int lengths [dim], T startVars [dim]);
		int SetDimMods (unsigned int d, T Ustart, T Uend, T Umod, T Lstart, T Lend, T Lmod);
		T GetAvg (unsigned int d);
		T GetVar (unsigned int d);
		~KPerf ();
	};
	Kalman (void (*getFunction) (S delta, MatrixBase<T> &F), void (*getNoise) (S delta, MatrixBase<T> &Q));
	~Kalman ();
	S GetTimeTravel ();
	int SetP (MatrixBase<T> &in);
	int Setx (MatrixBase<T> &in);
	void SetTime (S in);
	int GetP (MatrixBase<T> &out);
	int Getx (MatrixBase<T> &out);
	void Filter (MatrixBase<T> &z, MatrixBase<T> &R, MatrixBase<T> &H, S timeStamp);
	void Filter (MatrixBase<T> &z, KPerf &RD, MatrixBase<T> &H, S timeStamp);
	void Filter (MatrixBase<T> &z, MatrixBase<T> &R, MatrixBase<T> &H, MatrixBase<T> &B, MatrixBase<T> &u, S timeStamp);
	void Filter (MatrixBase<T> &z, KPerf &RD, MatrixBase<T> &H, MatrixBase<T> &B, MatrixBase<T> &u, S timeStamp);
};




#endif // KALMAN_HPP
