/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef ESF_HPP
#define ESF_HPP 1


/**
 * @brief Template class for ExponentialSmoothing objects.
 * T is the type of the state values, O is the type of the filter structure and S is the time system.
 * (note: O must support multiplication and addition with T, and O must support all operators with itself and S)
 */
template <class T = double, class D = double>
class ExponentialSmoother {
private:
	T prevLowOut;
	T prevHighOut;
	D exponent;
public:
	ExponentialSmoother (void);
	ExponentialSmoother (D exp);
	ExponentialSmoother (D exp, T prevLow, T prevHigh);
	~ExponentialSmoother ();
	void Filter (T in);
	T FilterLow (T in);
	T FilterHigh (T in);
	T GetLow ();
	T GetHigh ();
	void SetExp (D in);
	D GetExp ();
	void SetLow (T in);
	D GetTimeConst (D dt);
	void SetTimeConst (D in, D dt);
	D GetCutOffFreq (D dt);
	void SetCutOffFreq (D in, D dt);
};


/**
 * @brief Template class for ExponentialSmoothing objects.
 * T is the type of the state values, O is the type of the filter structure and S is the time system.
 * (note: O must support multiplication and addition with T, and O must support all operators with itself and S)
 */
#ifdef MULTI_EXP
template <class T = double, class D = double, unsigned int N = MULTI_EXP>
#else
template <class T = double, class D = double, unsigned int N = 2>
#endif
class MultiExponentialSmoother: public ExponentialSmoother<T, D> {
private:
	T prev;
	ExponentialSmoother<T, D> filterArray [N-1];
public:
	MultiExponentialSmoother (D exp);
	MultiExponentialSmoother (D exp, T prevLow, T prevHigh);
	~MultiExponentialSmoother ();
	void Filter (T in);
	T FilterLow (T in);
	T FilterHigh (T in);
	T GetLow ();
	T GetHigh ();
	void SetExp (D in);
	void SetLow (T in);
};


// Default instantiation.
template class ExponentialSmoother<>;
template class MultiExponentialSmoother<>;
template class ExponentialSmoother<float>;
template class MultiExponentialSmoother<float>;
#endif

