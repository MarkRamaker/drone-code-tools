/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef QUATERNION_HPP
#define QUATERNION_HPP 1

#define radiansFromDegrees(x) ((x) * 0.01745329251)
#define degreesFromRadians(x) ((x) * 57.2957795131)

#include <matrix.hpp>

/**
 * @brief Template class for Quaternion objects.
 */
template <class T = double>
class Quat  {
public:
private:
	T i [3];
	T r;
	Quat SlerpHelper (const Quat &in, T fraction, T dot) const;
	Quat LerpHelper (const Quat &in, T fraction) const;
public:
	Quat (void);
	Quat (MatrixBase<T> &rotMat);
	Quat (T yaw, T roll, T pitch);
	Quat (T real, MatrixBase<T> &vect);
	Quat (const Quat &in);
	Quat (T real, T im1, T im2, T im3);
	virtual ~Quat (void);
	T Normalize ();
	void Get (T &real, T &i1, T &i2, T &i3) const;
	T GetReal () const;
	T GetNorm () const;
	T Dot (const Quat &in) const;
	int GetVect (MatrixBase<T> &out) const;
	void SetReal (T in);
	int SetVect (MatrixBase<T> &in);
	Quat GetConjugate () const;	
	Quat GetInverse () const;
	Quat GetDoubleCover () const;
	Quat LeftMultiply (const Quat &inR) const;
	Quat RightMultiply (const Quat &inL) const;
	Quat DevideBy (const Quat &in) const;
	Quat GetSlerp (const Quat &in, T fraction) const;
	Quat GetLerp (const Quat &in, T fraction) const;
	Quat GetSlerpInc (const Quat &in, T fraction) const;
	Quat GetLerpInc (const Quat &in, T fraction) const;
	void RotateVector (MatrixBase<T> &vector) const;
	void GetRotationMatrix (MatrixBase<T> &out) const;
	void GetAngles (T &yaw, T &roll, T &pitch) const;
	// yaw.. roll.. pitch?
	
	// Overloaded Operators - could possibly deviate from normal quaternion math.
	Quat& operator += (Quat const &q);
	Quat& operator -= (Quat const &q);
	Quat& operator /= (Quat const &q);
	Quat& operator /= (T const d);
	Quat& operator *= (T const m);
	Quat& operator =  (Quat q);
	

	#ifdef DEBUGDEF
	void Print (char const * str);
	void PrintAngles (char const * str);
	void PrintAnglesDegrees (char const * str);
	#endif
};


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T = double>
Quat<T> operator + (Quat<T> const &ql, Quat<T> qr) {
	return qr += ql;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T = double>
Quat<T> operator - (Quat<T> ql, Quat<T> const &qr) {
	return ql -= qr;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T = double>
Quat<T> operator / (Quat<T> ql, Quat<T> const &qr) {
	return ql /= qr;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T = double>
Quat<T> operator / (Quat<T> ql, T const d) {
	return ql /= d;
}


/**
 * @brief Prints values of a matrix to stdout->
 * @param x (I) The matrix which will be copied from.
 * @param o (O) The matrix which will be copied to.
 */
template <class T = double>
Quat<T> operator * (Quat<T> ql, T const m) {
	return ql *= m;
}

	
// Default class instantion.
template class Quat<>;
template class Quat<float>;
#endif // QUATERNION_HPP
