/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef ORIENTF_HPP
#define ORIENTF_HPP 1
#include <quaternion.hpp>


/**
 * @brief Template class for PositionAccelerationFilter objects.
 * T is the type of the state values, O is the type of the filter structure and S is the time system.
 * (note: O must support multiplication and addition with T, and O must support all operators with itself and S)
 */
template <class T = double>
class AHRS {
private:
	T dt;
	T expg;
	T expa;
	Quat<T> gb;
	Quat<T> pref;
	Quat<T> prev;
public:
	AHRS (T expgyr, T expacc, T frq);
	AHRS (T expgyr, T expacc, T frq, Quat<T> ref, Quat<T> gyro);
	~AHRS ();
	Quat<T> Filter (T acc [3], T mag [3], T gyro [3]);
	void SetExpAcc (T in);
	void SetExpGyr (T in);
	T GetTimeConstAcc ();
	T GetTimeConstGyr ();
	void SetTimeConstAcc (T in);
	void SetTimeConstGyr (T in);
	T GetCutOffFreqAcc ();
	T GetCutOffFreqGyr ();
	void SetCutOffFreqAcc (T in);
	void SetCutOffFreqGyr (T in);
};



// Basetype.
template class AHRS<>;
template class AHRS<float>;
#endif
