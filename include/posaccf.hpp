/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef POSACCF_HPP
#define POSACCF_HPP 1
#include <esf.hpp>
#ifndef POS_ACC_VER
#define POS_ACC_VER 0
#endif

/**
 * @brief Template class for PositionAccelerationFilter objects.
 * T is the type of the state values, O is the type of the filter structure and S is the time system.
 * (note: O must support multiplication and addition with T, and O must support all operators with itself and S)
 */
template <class T = double>
class PositionAccelerationFilter {
private:
	T U;
	T P1;
	#if POS_ACC_VER == 0
	T P2;
	#endif
	#ifndef MULTI_EXP
	ExponentialSmoother<T, T> l1;
	ExponentialSmoother<T, T> l2;
	ExponentialSmoother<T, T> l3;
	ExponentialSmoother<T, T> h1;
	ExponentialSmoother<T, T> h2;
	ExponentialSmoother<T, T> h3;
	#else
	#if POS_ACC_VER == 1
	ExponentialSmoother<T, T> l1;
	ExponentialSmoother<T, T> l2;
	MultiExponentialSmoother<T, T, MULTI_EXP> l3;
	ExponentialSmoother<T, T> h1;
	ExponentialSmoother<T, T> h2;
	MultiExponentialSmoother<T, T, MULTI_EXP> h3;
	#elif POS_ACC_VER == 2
	ExponentialSmoother<T, T> l1;
	MultiExponentialSmoother<T, T, MULTI_EXP> l2;
	ExponentialSmoother<T, T> l3;
	ExponentialSmoother<T, T> h1;
	MultiExponentialSmoother<T, T, MULTI_EXP> h2;
	ExponentialSmoother<T, T> h3;
	#else
	MultiExponentialSmoother<T, T, MULTI_EXP> l1;
	ExponentialSmoother<T, T> l2;
	ExponentialSmoother<T, T> l3;
	MultiExponentialSmoother<T, T, MULTI_EXP> h1;
	ExponentialSmoother<T, T> h2;
	ExponentialSmoother<T, T> h3;
	#endif
	#endif
public:
	PositionAccelerationFilter (T exp, T frq);
	PositionAccelerationFilter (T exp, T frq, T pos, T acc);
	~PositionAccelerationFilter ();
	T Filter (T pos, T acc);
	void SetExp (T in);
	void SetExp1 (T in);
	void SetExp2 (T in);
	void SetExp3 (T in);
	T GetTimeConst1 ();
	T GetTimeConst2 ();
	T GetTimeConst3 ();
	void SetTimeConst1 (T in);
	void SetTimeConst2 (T in);
	void SetTimeConst3 (T in);
	T GetCutOffFreq1 ();
	T GetCutOffFreq2 ();
	T GetCutOffFreq3 ();
	void SetCutOffFreq1 (T in);
	void SetCutOffFreq2 (T in);
	void SetCutOffFreq3 (T in);
};



// Basetype.
template class PositionAccelerationFilter<>;
template class PositionAccelerationFilter<float>;
#endif
