/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef MATRIX_HPP
#define MATRIX_HPP 1
#include <initializer_list>

/* Error definitions */
#define SUCCESS 0
#define OUT_ERR -10 // The output matrix is of incorrect size.
#define SIZ_ERR -11 // Incompatible matrix sizes used.
#define INV_ERR -12 // Not invertible.
#define FOR_ERR -13 // Format error.

/**
 * @brief Template class for Matrix objects.
 */
template <class T = double>
class MatrixBase {
private:
	const unsigned int SIZE;
	T* const DATA;
	int Sweep (MatrixBase &in) const;
protected:
	unsigned int cols;
	unsigned int rows;
public:
	MatrixBase (unsigned int thisSize, T* thisData, unsigned int thisRows, unsigned int thisCols);
	MatrixBase (T* thisData, unsigned int thisRows, unsigned int thisCols);
	virtual ~MatrixBase (void);
	int SetDims (unsigned int rows, unsigned int cols);
	int SetDimsDumb (unsigned int rows, unsigned int cols);
	unsigned int GetRows () const;
	unsigned int GetCols () const;
	int Copy (MatrixBase &out) const;
	int ScalarMult (T in, MatrixBase &out) const;
	int LeftMult (MatrixBase const &inR, MatrixBase &out) const;
	int RightMult (MatrixBase const &inL, MatrixBase &out) const;
	int LeftSubtract (MatrixBase const &in, MatrixBase &out) const;
	int RightSubtract (MatrixBase const &in, MatrixBase &out) const;
	int Addition (MatrixBase const &in, MatrixBase &out) const;
	int InnerProd (MatrixBase const &in, T &out) const;
	int LeftCrossProd (MatrixBase const &inR, MatrixBase &out) const;
	int RightCrossProd (MatrixBase const &inL, MatrixBase &out) const;
	int Inverted (MatrixBase &out) const;
	int Transposed (MatrixBase &out) const;
	int Determinant (T &det) const;
	T GetElement (unsigned int row, unsigned int col) const;
	int GetRow (unsigned int row, MatrixBase &out) const;
	int GetCol (unsigned int col, MatrixBase &out) const;
	int SetElement (unsigned int row, unsigned int col, T in);
	int SetRow (unsigned int row, MatrixBase &in);
	int SetCol (unsigned int col, MatrixBase &in);
	int SetDiagonal (MatrixBase &in);
	int RightSweep (MatrixBase const &inL, MatrixBase &out) const;
	int LeftSweep (MatrixBase const &inR, MatrixBase &out) const;
	int SwapRows (unsigned int thisRow, unsigned int outRow, MatrixBase &out);
	int SwapCols (unsigned int thisCol, unsigned int outCol, MatrixBase &out);
	int ScalarDivRow (unsigned int thisRow, unsigned int outRow, T div, MatrixBase &out);
	int ScalarDivCol (unsigned int thisCol, unsigned int outCol, T div, MatrixBase &out);
	int SubtractRows (unsigned int thisRow, unsigned int outRow, T mult, MatrixBase &out);
	int SubtractCols (unsigned int thisCol, unsigned int outCol, T mult, MatrixBase &out);
	#ifdef DEBUGDEF
	void Print (char const * str);
	#endif
};

/**
 * @brief Template class for Matrix objects.
 */
template <class T = double, unsigned int r = 9, unsigned int c = 9>
class Matrix : public MatrixBase<T>  {
private:
	T d [r * c];
public:
	Matrix ();
	Matrix (unsigned int rows, unsigned int cols);
	Matrix (std::initializer_list<std::initializer_list<T>> in);
	Matrix (Matrix const &in);
	Matrix& operator = (std::initializer_list<std::initializer_list<T>> in);
	Matrix& operator = (Matrix const &in);
	Matrix& operator += (Matrix const &in);
	Matrix& operator -= (Matrix const &in);
	Matrix& operator *= (Matrix const &in);
	Matrix& operator *= (T in);
	Matrix& operator /= (T in);
};


/**
 * @brief Add two matrix objects.
 * @param ml (I/O) The left matrix.
 * @param mr (I) The right matrix.
 * @return the result.
 */
template <class T = double, unsigned int r = 9, unsigned int c = 9>
Matrix<T, r, c>  operator + (Matrix<T, r, c> ml, Matrix<T, r, c> const &mr) {
	return ml += mr;
}


/**
 * @brief Subtract two matrix objects.
 * @param ml (I/O) The left matrix.
 * @param mr (I) The right matrix.
 * @return the result.
 */
template <class T = double, unsigned int r = 9, unsigned int c = 9>
Matrix<T, r, c>  operator - (Matrix<T, r, c> ml, Matrix<T, r, c> const &mr) {
	return ml -= mr;
}


/**
 * @brief Multiply two matrix objects.
 * @param ml (I/O) The left matrix.
 * @param mr (I) The right matrix.
 * @return the result.
 */
template <class T = double, unsigned int r = 9, unsigned int c = 9>
Matrix<T, r, c>  operator * (Matrix<T, r, c> ml, Matrix<T, r, c> const &mr) {
	return ml *= mr;
}


/**
 * @brief Multiply a matrix by a scalar.
 * @param ml (I/O) The left matrix.
 * @param mr (I) The scalar operand.
 * @return the result.
 */
template <class T = double, unsigned int r = 9, unsigned int c = 9>
Matrix<T, r, c>  operator * (Matrix<T, r, c> ml, T mr) {
	return ml *= mr;
}


/**
 * @brief Devide a matrix by a scalar.
 * @param ml (I/O) The left matrix.
 * @param mr (I) The scalar operand.
 * @return the result.
 */
template <class T = double, unsigned int r = 9, unsigned int c = 9>
Matrix<T, r, c>  operator / (Matrix<T, r, c> ml, T mr) {
	return ml /= mr;
}


/**
 * @brief Return the inverse of a matrix.
 * @param mr (I) The matrix.
 * @return the result.
 */
template <class T = double, unsigned int r = 9, unsigned int c = 9>
Matrix<T, r, c>  operator ~ (Matrix<T, r, c> const &mr) {
	Matrix<T, r, c> o (mr.GetRows (), mr.GetCols ());
	mr.Inverted (o);
	return o;
}


/**
 * @brief Return the transpose of a matrix.
 * @param mr (I) The matrix.
 * @return the result.
 */
template <class T = double, unsigned int r = 9, unsigned int c = 9>
Matrix<T, r, c>  operator ! (Matrix<T, r, c> const &mr) {
	Matrix<T, r, c> o (mr.GetCols (), mr.GetRows ());
	mr.Transposed (o);
	return o;
}


//Default class instantiation.
template class MatrixBase<>;
template class Matrix<>;
template class MatrixBase<float>;
template class Matrix<float>;
#endif // MATRIX_HPP
