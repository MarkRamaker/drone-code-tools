/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain this copyright
 *    notice, this list of conditions and all other provided disclaimers.
 * 2. Redistributions in binary form must reproduce this copyright
 *    notice, this list of conditions and all other provided disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the authors name nor the names of the contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>
#include <quaternion.hpp>
#include <cmath>

#ifndef UNSAFE_QUAT
#define SAFE(x) x
#else
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2
#define SMALL 0.000005 // Might be compiler / math lib dependant


/**
 * @brief Helper function to get the spherical linear interpolated quaternion between this and in.
 * @param in (I) The far side quaternion. 
 * @param fraction (I) A number between 0 and 1 that scales between (0) this and (1) in.
 * @param dot (I) The cosine of the angle between this and in.
 * @return The spherical linear interpolated quaternion.
 */
template <class T>
Quat<T> Quat<T>::SlerpHelper (const Quat &in, T fraction, T dot) const {
	T angle = acos (dot);
	Quat d = *this;
    
	// Get the Slerp
    T s1 = sin ((ONE - fraction) * angle) / sin (angle);
    T s2 = sin (fraction * angle) / sin (angle);
    d.r = this->r * s1 + in.r * s2;
    d.i [DIM_1] = this->i [DIM_1] * s1 + in.i [DIM_1] * s2;
    d.i [DIM_2] = this->i [DIM_2] * s1 + in.i [DIM_2] * s2;
    d.i [DIM_3] = this->i [DIM_3] * s1 + in.i [DIM_3] * s2;
	return d;
}

// End

