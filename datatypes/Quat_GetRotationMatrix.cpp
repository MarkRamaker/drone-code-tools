/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>
#include <quaternion.hpp>
#include <cmath>

#ifndef UNSAFE_QUAT
#define SAFE(x) x
#else
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2
#define SMALL 0.000000005 // Might be compiler / math lib dependant


/**
 * @brief Convert a unit quaternion to a rotation matrix and returns the result.
 * @param out (O) The output rotation matrix.
 */
template <class T>
void Quat<T>::GetRotationMatrix (MatrixBase<T> &out) const {
    out.SetDims(THREE, THREE);
    T q1 = this->r;
    T q2 = this->i [DIM_1];
    T q3 = this->i [DIM_2];
    T q4 = this->i [DIM_3];
	T q1s = q1 * q1;
	T q2s = q2 * q2;
	T q3s = q3 * q3;
	T q4s = q4 * q4;
	
	// Diagonal.
	out.SetElement(DIM_1, DIM_1, q1s + q2s - q3s - q4s);
	out.SetElement(DIM_2, DIM_2, q1s - q2s + q3s - q4s);
	out.SetElement(DIM_3, DIM_3, q1s - q2s - q3s + q4s);
	
	// The rest.
	out.SetElement(DIM_2, DIM_1, TWO * (q2 * q3 + q1 * q4));
	out.SetElement(DIM_1, DIM_2, TWO * (q2 * q3 - q1 * q4));
	out.SetElement(DIM_3, DIM_1, TWO * (q2 * q4 - q1 * q3));
	out.SetElement(DIM_1, DIM_3, TWO * (q2 * q4 + q1 * q3));
	out.SetElement(DIM_3, DIM_2, TWO * (q3 * q4 + q1 * q2));
	out.SetElement(DIM_2, DIM_3, TWO * (q3 * q4 - q1 * q2));
}

// End

