/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>

#ifndef UNSAFE_MATRIX
#ifdef ASSERTED_MATRIX
#include <assert.h>
#define SAFE(x) //empty
#define ASAFE(x) x
#else
#define ASAFE(x) //empty
#define SAFE(x) x
#endif
#else
#define ASAFE(x) //empty
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2


/**
 * @brief Performs a gaussian row reduction to reduced echelon form with this object to the left.
 * @param inR (I) The matrix which will be put to the right durring reduction.
 * @param out (O) The matrix where the right side of the reduced echelon form will be put.
 * @return SIZ_ERR if this object and inR do not match dimensions, FOR_ERR has wrong dimensions, otherwise returns the amount of unswept rows.
 */
template <class T>
int MatrixBase<T>::LeftSweep (MatrixBase const &inR, MatrixBase &out) const {
	SAFE( if (this->rows != inR.rows || this->cols != inR.cols) return SIZ_ERR );
	SAFE( if (this->rows != out.rows || this->cols != out.cols) return FOR_ERR );
	ASAFE( assert (!(this->rows != inR.rows || this->cols != inR.cols)) );
	ASAFE( assert (!(this->rows != out.rows || this->cols != out.cols)) );
	int ret;
	T store [this->rows * this->cols * DIM_3];
	MatrixBase<T> sweeper (this->rows * this->cols * DIM_3, store, this->rows, this->cols * DIM_3);

	// Copy matrices to the sweeper matrix.
	for (unsigned int r = DIM_1; r < this->rows; r++) for (unsigned int c = DIM_1; c < this->cols; c++) {
		sweeper.SetElement (r, c + this->cols, inR.GetElement(r, c));
		sweeper.SetElement (r, c, this->GetElement (r, c));
	}

	// Sweep the matrix.
	ret = this->Sweep (sweeper);

	// Copy the result to the output matrix.
	for (unsigned int r = DIM_1; r < this->rows; r++) for (unsigned int c = DIM_1; c < this->cols; c++) {
		out.SetElement (r, c, sweeper.GetElement(r, c + this->cols));
	}

	return ret;
}

// End

