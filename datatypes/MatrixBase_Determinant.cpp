/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>

#ifndef UNSAFE_MATRIX
#ifdef ASSERTED_MATRIX
#include <assert.h>
#define SAFE(x) //empty
#define ASAFE(x) x
#else
#define ASAFE(x) //empty
#define SAFE(x) x
#endif
#else
#define ASAFE(x) //empty
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2


/**
 * @brief This function calculates the determinant of a square matrix.
 * @param det (O) Ouput of the calculated determinant when SUCCESS is returned.
 * @return SIZ_ERR If the matrix is not square or INV_ERR If the matrix can not be inverted, otherwise return SUCCESS.
 */
template <class T>
int MatrixBase<T>::Determinant (T &det) const {
	SAFE( if (this->rows != this->cols) return SIZ_ERR );
	ASAFE( assert (!(this->rows != this->cols)) );
	T store [this->rows * this->cols];
	MatrixBase<T> rowEchelon (this->rows * this->cols, store, this->rows, this->cols);
	this->Copy (rowEchelon);
	T qot = ONE;

	// Work our way down the rows. Moving rows if we need to.
	for (unsigned int r = DIM_1; r < rowEchelon.rows; r++) {
		unsigned int rs, s;
		// Find out where to swap.
		for (rs = r + ONE, s = r; rs < rowEchelon.rows; rs++) {
			if (rowEchelon.GetElement (rs, r) > rowEchelon.GetElement (s, r)) s = rs;
		}
		// No row to swap so stop here.
		if (rowEchelon.GetElement (s, r) == ZERO) {
			return INV_ERR;
		}	else {
			// Swapping two rows changes the sign of the det.
			rowEchelon.SwapRows (r, s, rowEchelon);
			qot *= -ONE;
		}

		// Normalize the row. FIXME close to ZERO check?
		// Multiplying a row by a scalar multiplies the det by that same scalar.
		qot /= rowEchelon.GetElement (r, r);
		rowEchelon.ScalarDivRow (r, r, rowEchelon.GetElement (r, r), rowEchelon);

		// Now run down this collumn.
		for (unsigned int rr = r + ONE; rr < rowEchelon.rows; rr++) {
			rowEchelon.SubtractRows (r, rr, rowEchelon.GetElement (rr, r), rowEchelon);
		}
	}

	// Det = prod( diag(rowEchelon)) / qot. Since diag is all ones,
	// prod( diag(rowEchelon)) == 1. FIXME check close to zero?
	if (qot == ZERO) return INV_ERR;
	det = ONE / qot;
	return SUCCESS;
}

// End

