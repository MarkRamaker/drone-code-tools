/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>
#include <quaternion.hpp>
#include <cmath>

#ifndef UNSAFE_QUAT
#define SAFE(x) x
#else
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2
#define SMALL 0.000000005 // Might be compiler / math lib dependant


/**
 * @brief Quaternion default constructor.
 */
template <class T>
Quat<T>::Quat (void) {
	this->r = ONE;
	this->i [DIM_1] = ZERO;
	this->i [DIM_2] = ZERO;
	this->i [DIM_3] = ZERO;
}


/**
 * @brief Quaternion rotation matrix constructor.
 * @param rotMat (I) The rotation matrix. 
 * http://www.iri.upc.edu/files/scidoc/2068-Accurate-Computation-of-Quaternions-from-Rotation-Matrices.pdf
 */
template <class T>
Quat<T>::Quat (MatrixBase<T> &rotMat) {
    const T TRACELIM = ZERO;
    T d11 = rotMat.GetElement (DIM_1, DIM_1);
    T d12 = rotMat.GetElement (DIM_1, DIM_2);
    T d13 = rotMat.GetElement (DIM_1, DIM_3);
    T d21 = rotMat.GetElement (DIM_2, DIM_1);
    T d22 = rotMat.GetElement (DIM_2, DIM_2);
    T d23 = rotMat.GetElement (DIM_2, DIM_3);
    T d31 = rotMat.GetElement (DIM_3, DIM_1);
    T d32 = rotMat.GetElement (DIM_3, DIM_2);
    T d33 = rotMat.GetElement (DIM_3, DIM_3);
    T q1, q2, q3, q4;
    
    // Determine q1.
    if (d11 + d22 + d33 > TRACELIM) {
        q1 = 0.5 * sqrt(ONE + d11 + d22 + d33);
    } else {
        T sq1 = d32 - d23;
        T sq2 = d13 - d31;
        T sq3 = d21 - d12;
        sq1 *= sq1;
        sq2 *= sq2;
        sq3 *= sq3;
        q1 = 0.5 * sqrt((sq1 + sq2 + sq3) / (THREE - d11 - d22 - d33));
    }
    
    // Determine q2.
    if (d11 - d22 - d33 > TRACELIM) {
        q2 = 0.5 * sqrt(ONE + d11 - d22 - d33);
    } else {
        T sq1 = d32 - d23;
        T sq2 = d12 + d21;
        T sq3 = d31 + d13;
        sq1 *= sq1;
        sq2 *= sq2;
        sq3 *= sq3;
        q2 = 0.5 * sqrt((sq1 + sq2 + sq3) / (THREE - d11 + d22 + d33));
    }
    
    // Determine q3.
    if (-d11 + d22 - d33 > TRACELIM) {
        q3 = 0.5 * sqrt(ONE - d11 + d22 - d33);
    } else {
        T sq1 = d13 - d31;
        T sq2 = d12 + d21;
        T sq3 = d23 + d32;
        sq1 *= sq1;
        sq2 *= sq2;
        sq3 *= sq3;
        q3 = 0.5 * sqrt((sq1 + sq2 + sq3) / (THREE + d11 - d22 + d33));
    }
    
    // Determine q4.
    if (-d11 - d22 + d33 > TRACELIM) {
        q4 = 0.5 * sqrt(ONE - d11 - d22 + d33);
    } else {
        T sq1 = d21 - d12;
        T sq2 = d31 + d13;
        T sq3 = d32 + d23;
        sq1 *= sq1;
        sq2 *= sq2;
        sq3 *= sq3;
        q4 = 0.5 * sqrt((sq1 + sq2 + sq3) / (THREE + d11 + d22 - d33));
    }
    
    // Set the signs for the imaginary vector.
    q2 *= d32 - d23 > ZERO ? ONE : -ONE;
    q3 *= d13 - d31 > ZERO ? ONE : -ONE;
    q4 *= d21 - d12 > ZERO ? ONE : -ONE;
    
	// Initialize the quat.
	this->r = q1;
	this->i [DIM_1] = q2;
	this->i [DIM_2] = q3;
	this->i [DIM_3] = q4;
}


/**
 * @brief Quaternion angular distance constructor.
 * @param yaw (I) The yaw distance. Positive values represent clockwise rotation.
 * @param roll (I) The roll distance. Positive values represent clockwise rotation.
 * @param pitch (I) The pitch distance. Positive values represent clockwise rotation.
 */
template <class T>
Quat<T>::Quat (T yaw, T roll, T pitch) {
	// Are pitch, yaw and roll in the right order?
	T mag = sqrt (pitch * pitch + yaw * yaw + roll * roll);
	if (mag != ZERO) { 
	    T s = sin (mag / TWO) / mag;
	    this->r = cos (mag / TWO);
	    this->i [DIM_1] = pitch * s;
	    this->i [DIM_2] = yaw * s;
	    this->i [DIM_3] = roll * s;
    } else {
        this->r = ONE;
	    this->i [DIM_1] = ZERO;
	    this->i [DIM_2] = ZERO;
	    this->i [DIM_3] = ZERO;
    }
}


/**
 * @brief Quaternion copy constructor.
 * @param in (I) Quaternion to copy. 
 */
template <class T>
Quat<T>::Quat (const Quat &in) {
	this->r = in.r;
	this->i [DIM_1] = in.i [DIM_1];
	this->i [DIM_2] = in.i [DIM_2];
	this->i [DIM_3] = in.i [DIM_3];
}


/**
 * @brief Quaternion value constructor.
 * @param real (I) Real part of the quaternion. 
 * @param in1 (I) The "i" part of the quaternion.
 * @param in2 (I) The "j" part of the quaternion.
 * @param in3 (I) The "k" part of the quaternion.
 */
template <class T>
Quat<T>::Quat (T real, T im1, T im2, T im3) {
	this->r = real;
	this->i [DIM_1] = im1;
	this->i [DIM_2] = im2;
	this->i [DIM_3] = im3;
}


/**
 * @brief Quaternion real,vector constructor. 
 * @param real (I) Real part of the quaternion. 
 * @param vec (I) The vector part of the quaternion.
 */
template <class T>
Quat<T>::Quat (T real, MatrixBase<T> &vect) {
	this->r = r;
	MatrixBase<T> t (THREE, this->i, ONE, THREE); 
	vect.Copy (t); 
}

// End

