/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>

#ifndef UNSAFE_MATRIX
#ifdef ASSERTED_MATRIX
#include <assert.h>
#define SAFE(x) //empty
#define ASAFE(x) x
#else
#define ASAFE(x) //empty
#define SAFE(x) x
#endif
#else
#define ASAFE(x) //empty
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2


/**
 * @brief General matrix constructor.
 */
template <class T, unsigned int r, unsigned int c>
Matrix<T, r, c>::Matrix ()
: MatrixBase<T>(r * c, d, r, c) {

}


/**
 * @brief Matrix constructor sets the matrix to a specific size.
 * @param rows (I) The amount of rows to set the matrix to.
 * @param cols (I) The amoiunt of column to set the matrix to.
 */
template <class T, unsigned int r, unsigned int c>
Matrix<T, r, c>::Matrix (unsigned int rows, unsigned int cols)
: MatrixBase<T>(r * c, d, rows, cols) {

}



/**
 * @brief Matrix contructor based on initializer list.
 * @param in (I) Two dimensional inistializer list.
 */
template <class T, unsigned int r, unsigned int c>
Matrix<T, r, c>::Matrix (std::initializer_list<std::initializer_list<T>> in)
: MatrixBase<T>(r * c, d, in.size (), in.begin ()->size ()) {
    for (unsigned int rr = 0; rr < this->rows; rr++) {
		for (unsigned int cc = 0; cc < this->cols; cc++) { 
			d [cc + rr * this->cols] = in.begin () [rr].begin ()[cc];  
		}
	}	
}

/**
 * @brief Matrix copy contructor.
 * @param in (I) The copy matrix.
 */
template <class T, unsigned int r, unsigned int c>
Matrix<T, r, c>::Matrix (Matrix const &in)
: MatrixBase<T>(r * c, d, in.rows, in.cols) {
    in.Copy (*this);
}

// End

