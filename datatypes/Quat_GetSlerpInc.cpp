/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>
#include <quaternion.hpp>
#include <cmath>

#ifndef UNSAFE_QUAT
#define SAFE(x) x
#else
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2
#define TH 0.9995 // Might be compiler / math lib dependant


/**
 * @brief Get a local frame quaternion that describes an incremental spherical linear interpolated step between this and in.
 * @param in (I) The far side quaternion. 
 * @param fraction (I) A number between 0 and 1 that scales between (0) this and (1) in.
 * @return The incremental spherical linear interpolating quaternion.
 */
template <class T>
Quat<T> Quat<T>::GetSlerpInc (const Quat &in, T fraction) const {
	T dot = this->Dot (in);
	Quat d;
	Quat z;
	
	// For no fraction return zero quat.
	if (fraction == ZERO) return d;
	
	// For shortest path we need dot to be positive.
	// If this is not the case, inverting one of the quaternions
	// will give the shortest path.
	// A full inverse of a unit quaternion is equivalent to it's non
	// inverted counterpart.
	if (dot < ZERO) {
		// Invert 
		d = this->GetDoubleCover ();
		dot = -dot;
	} else d = *this;
	
	// Get difference quaternion.
	d = d.GetConjugate ();
	d = in.RightMultiply(d);
	
	// If inputs are too close do linear interpolation.
	if (dot < TH) return z.LerpHelper (d, fraction);

	// Clamp dot and then slerp.
	dot = dot > ONE ? ONE : dot;
	return z.SlerpHelper (d, fraction, dot);
}

// End

