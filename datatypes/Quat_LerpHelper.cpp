/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>
#include <quaternion.hpp>
#include <cmath>

#ifndef UNSAFE_QUAT
#define SAFE(x) x
#else
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2
#define SMALL 0.0000000005 // Might be compiler / math lib dependant


/**
 * @brief Helper function to get the linear interpolated quaternion between this and in.
 * @param in (I) The far side quaternion. 
 * @param fraction (I) A number between 0 and 1 that scales between (0) this and (1) in.
 * @return The linear interpolated quaternion.
 */
template <class T>
Quat<T> Quat<T>::LerpHelper (const Quat &in, T fraction) const {
    Quat<T> d = *this;
	// Get the Lerp
	d.r = d.r * (ONE - fraction) + in.r * fraction;
	d.i [DIM_1] = d.i [DIM_1] * (ONE - fraction) + in.i [DIM_1] * fraction;
	d.i [DIM_2] = d.i [DIM_2] * (ONE - fraction) + in.i [DIM_2] * fraction;
	d.i [DIM_3] = d.i [DIM_3] * (ONE - fraction) + in.i [DIM_3] * fraction;
	
	// Lerp does not conserve unity so normalize.
    d.Normalize ();
	return d;
}

// End

