/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>
#include <quaternion.hpp>
#include <cmath>

#ifndef UNSAFE_QUAT
#define SAFE(x) x
#else
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2
#define SMALL 0.000000005 // Might be compiler / math lib dependant


/**
 * @brief Divide a quaternion by another quaternion. (Whatever that means.)
 * @param in (I) The quaternion to divide this quaternion by. 
 * @return The resulting quaternion. 
 * According to mathworks.com... quat devision.
 */
template <class T>
Quat<T> Quat<T>::DevideBy (const Quat &in) const {
	T d;
	T q0, q1, q2, q3;
	T r0, r1, r2, r3;
	
	d = this->Dot (in);
	q0 = this->r;
	q1 = this->i [DIM_1];
	q2 = this->i [DIM_2];
	q3 = this->i [DIM_3];
	r0 = in.r;
	r1 = in.i [DIM_1];
	r2 = in.i [DIM_2];
	r3 = in.i [DIM_3];
	
	Quat ret   ((r0 * q0 + r1 * q1 + r2 * q2 + r3 * q3) / d, 
	            (r0 * q1 - r1 * q0 - r2 * q3 + r3 * q2) / d,
	            (r0 * q2 + r1 * q3 - r2 * q0 - r3 * q1) / d, 
	            (r0 * q3 - r1 * q2 + r2 * q1 - r3 * q0) / d);
	return ret;
}

// End

