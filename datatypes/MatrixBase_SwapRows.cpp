/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>

#ifndef UNSAFE_MATRIX
#ifdef ASSERTED_MATRIX
#include <assert.h>
#define SAFE(x) //empty
#define ASAFE(x) x
#else
#define ASAFE(x) //empty
#define SAFE(x) x
#endif
#else
#define ASAFE(x) //empty
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2


/**
 * @brief Swaps a row from this object with a row from out.
 * @param thisRow (I) The row to swap from this object.
 * @param outRow (I) The row to swap from out.
 * @param out (I/O) The other matrix object from which to swap.
 * @return OUT_ERR if this object and out do not have equal column dimension or FOR_ERR if the rows are out of bounce, otherwise return SUCCESS.
 */
template <class T>
int MatrixBase<T>::SwapRows (unsigned int thisRow, unsigned int outRow, MatrixBase &out) {
	SAFE( if (this->cols != out.cols) return OUT_ERR );
	SAFE( if (this->rows <= thisRow || out.rows <= outRow) return FOR_ERR );
	ASAFE( assert (!(this->cols != out.cols)) );
	ASAFE( assert (!(this->rows <= thisRow || out.rows <= outRow)) );
	T temp;
	for (unsigned int c = DIM_1; c < this->cols; c++) {
		temp = this->GetElement (thisRow, c);
		this->SetElement (thisRow, c, out.GetElement (outRow, c));
		out.SetElement (outRow, c, temp);
	}
	return SUCCESS;
}

// End

