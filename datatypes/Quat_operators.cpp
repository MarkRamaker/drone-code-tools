/****************************************************************************
 *
 *   Copyright (C) 2020 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain the all copyright
 *    notices, this list of conditions and the all disclaimers.
 * 2. Redistributions in binary form must reproduce the all copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Mark Ramaker nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <matrix.hpp>
#include <quaternion.hpp>
#include <cmath>

#ifndef UNSAFE_QUAT
#define SAFE(x) x
#else
#define SAFE(x) //empty
#endif 

#define ZERO  (T)0
#define ONE   (T)1
#define TWO   (T)2
#define THREE (T)3
#define DIM_1 0
#define DIM_2 1
#define DIM_3 2
#define SMALL 0.000000005 // Might be compiler / math lib dependant


/**
 * @brief Operator += overload, q in local frame orientation addition.
 * @param q (I) The orientation in local frame to add.
 * @return The resulting quaternion (orientation).
 */
template <class T>
Quat<T>& Quat<T>::operator += (Quat<T> const &q) {
	return *this = this->LeftMultiply (q);
}


/**
 * @brief Operator -= overload, q in local frame orientation subtraction.
 * @param q (I) The orientation in local frame to subtract. 
 * @return The resulting quaternion (orientation).
 */
template <class T>
Quat<T>& Quat<T>::operator -= (Quat<T> const &q) {
	Quat<T> qm = q.GetConjugate ();
	return *this = this->RightMultiply (qm);
}


/**
 * @brief Operator /= overload, divides this quaternion by q.
 * @param q (I) The quaternion to divide by. 
 * @return The resulting quaternion (orientation).
 */
template <class T>
Quat<T>& Quat<T>::operator /= (Quat<T> const &q) {
	return *this = this->DevideBy (q);
}


/**
 * @brief Operator /= overload, takes a fraction of a quaternion local frame orientation.
 * @param d (I) The fraction. 
 * @return The resulting quaternion (orientation).
 */
template <class T>
Quat<T>& Quat<T>::operator /= (T const d) {
	Quat<T> q0;
	return *this = q0.GetSlerp (*this, 1 / d);
}


/**
 * @brief Operator *= overload, takes a scalar multiple of a quaternion local frame orientataion.
 * @param m (I) The scalar multiple.
 * @return The resulting quarternion (orientation).
 */
template <class T>
Quat<T>& Quat<T>::operator *= (T const m) {
	Quat<T> q0;
	return *this = q0.GetSlerp (*this, m);
}

/**
 * @brief Operator = overload, quaternion assignment.
 * @param q (I) The quaternion to assign.
 * @return The copied result.
 */
template <class T>
Quat<T>& Quat<T>::operator = (Quat q) {
	this->r = q.r;
	this->i [DIM_1] = q.i [DIM_1];
	this->i [DIM_2] = q.i [DIM_2];
	this->i [DIM_3] = q.i [DIM_3];
	return *this;
}

// End

